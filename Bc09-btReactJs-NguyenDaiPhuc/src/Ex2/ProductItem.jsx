import React, { Component } from "react";

class ProductItem extends Component {
  render() {
    const { url } = this.props.prod;
    return (
      <div className="">
        <div
          className="p-2"
          onClick={() => {
            this.props.thuKinh(this.props.prod);
          }}
        >
          <img
            style={{ height: "100%", width: "100%" }}
            src={url}
            alt="product"
          />
        </div>
      </div>
    );
  }
}

export default ProductItem;
