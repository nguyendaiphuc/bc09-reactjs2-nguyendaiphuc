import React, { Component } from "react";
import ProductItem from "./ProductItem";

class ProductList extends Component {
  renderProuduct = () => {
    const productsList = this.props.arrProduct.map((item) => {
      return (
        <div key={item.id} className="col-1 m-auto p-2">
          <ProductItem prod={item} thuKinh={this.props.thuKinh} />
        </div>
      );
    });
    return productsList;
  };
  render() {
    return (
      <div className="container">
        <div className="row bg-dark">{this.renderProuduct()}</div>
      </div>
    );
  }
}

export default ProductList;
