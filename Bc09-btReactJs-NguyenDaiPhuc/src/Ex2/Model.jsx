import React, { Component } from "react";
import "./model.css";
class Model extends Component {
  renderKinh = () => {
    return this.props.addProduct.map((item) => {
      const { url, desc, name } = item;
      return (
        <>
          <div className="imgKinh">
            <img src={url} alt="product" />
          </div>
          <div className="info text-left  ">
            <h4>{name}</h4>
            <p>{desc}</p>
          </div>
        </>
      );
    });
  };
  render() {
    return (
      <div
        className="wraper"
        style={{
          backgroundImage: "url(./assets/img/background.jpg)",
          backgroundRepeat: "no-repeat",
        }}
      >
        <div
          className="model"
          style={{ backgroundImage: "url(./assets/img/model.jpg)" }}
        >
          {this.renderKinh()}
        </div>
      </div>
    );
  }
}

export default Model;
