import React, { Component } from "react";

class GheItem extends Component {
  state = { isBooking: false };
  onClickButton = () => {
    this.props.datGheXe(this.props.danhSachGhe);
    this.setState((prevStare) => ({
      isBooking: !prevStare.isBooking,
    }));
  };
  renderButton = () => {
    const { SoGhe, TrangThai } = this.props.danhSachGhe;
    if (TrangThai) {
      return (
        <button
          disabled="disabled"
          onClick={this.onClickButton}
          style={{ width: 60, height: 60 }}
          className="btn btn-danger"
        >
          {SoGhe}
        </button>
      );
    } else {
      return (
        <button
          onClick={this.onClickButton}
          style={{ width: 60, height: 60 }}
          className={
            this.state.isBooking ? "btn btn-success" : "btn btn-secondary"
          }
        >
          {SoGhe}
        </button>
      );
    }
  };
  render() {
    return <div className="mb-5">{this.renderButton()}</div>;
  }
}

export default GheItem;
