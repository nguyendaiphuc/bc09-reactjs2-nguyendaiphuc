import React, { Component } from "react";
import GheItem from "./gheItem";

class DanhSachGhe extends Component {
  renderDanhSachGhe = () => {
    const dsGhe = this.props.dsGhe.map((item) => {
      return (
        <div key={item.SoGhe} className="col-3">
          <GheItem danhSachGhe={item} datGheXe={this.props.datGheXe} />
        </div>
      );
    });
    return dsGhe;
  };

  render() {
    return (
      <div className="container">
        <div className="row">{this.renderDanhSachGhe()}</div>
      </div>
    );
  }
}

export default DanhSachGhe;
