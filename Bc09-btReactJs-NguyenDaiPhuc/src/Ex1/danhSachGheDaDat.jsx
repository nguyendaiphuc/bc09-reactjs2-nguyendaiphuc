import React, { Component } from "react";

class DanhSachGheDaDat extends Component {
  renderGheDaDat = () => {
    return this.props.danhSachGheDangDat.map((item) => {
      const { TenGhe, Gia } = item.danhSach;
      return (
        <div>
          Ghế: {TenGhe}: Giá:{Gia}
        </div>
      );
    });
  };
  render() {
    return (
      <div>
        <h4 className="text-warning"> Các Ghế Đã Đặt</h4>
        {this.renderGheDaDat()}
      </div>
    );
  }
}

export default DanhSachGheDaDat;
